
let App=function(){
    "use strict";

    return {
        init: function(options){
            this.initBase(options);
            this.initComponents(options);
            this.initPages(options);

        },
        initBase: function(options){

        },
        initComponents: function(options){

        },
        initPages: function(options){
            contactForm(options);
            handleSlideToggle(options);
            printGiftCard(options);

        }
    }
}();