
let printGiftCard = function (options) {
    if (options.getCard) {
        let buttonCard = options.getCard.buttonCard;
        let event = options.getCard.event;
        let printEl = options.getCard.printEl;
        let buttonCancel = options.getCard.buttonCancel;
        
        $(buttonCard).on(event, function (e) {
            $(printEl).addClass('card-color');
            $(buttonCancel).show();

        });

        $(buttonCancel).on(event, function (e) {
            $(printEl).removeClass('card-color');
            $(buttonCancel).hide();

        });

    }
}