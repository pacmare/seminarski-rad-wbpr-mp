let initPosition;

function initMap() {
    var lat = 45.805373;
    var lng = 15.981453;
    var mapBox = document.getElementById('map');
    var colorMarker="green";
    var contentString='<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<div id="bodyContent">' +
    "<p>Lokacija salona<br>Trg Kralja Tomislava 12<br>10000 Zagreb</p>" +
    "</div>" +
    "</div>";

    var infoWindow=new google.maps.InfoWindow({content: contentString});

    var mapOptions = {
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(mapBox, mapOptions);
    initPosition = new google.maps.LatLng(lat, lng);
    addMarker(initPosition,colorMarker);

    function addMarker(latLng, color) {
        let url = "http://maps.google.com/mapfiles/ms/icons/";
        url += color + "-dot.png";
      
        let marker = new google.maps.Marker({
          map: map,
          position: latLng,
          title: "Lokacija salona!",
          icon: {
            url: url
          }
        });
      marker.addListener("click", ()=>{
        infoWindow.open(map, marker);
      });
    }

    map.setCenter(initPosition);
}
