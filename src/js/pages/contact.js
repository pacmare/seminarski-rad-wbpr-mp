let contactForm=function(options){
    if(options.contactForm){
        let resetButton=options.contactForm.resetButton;
        let form=options.contactForm.form;
        let toggleEl=options.slideToggle.toggleEl;

        $(resetButton).on('click', function(e){
            e.preventDefault();
            $(form)[0].reset();
            $(form).parsley().reset();
            $(toggleEl).slideUp();
        })
    }
        
}

let handleSlideToggle = function (options) { 
    if(options.slideToggle){
        let listenerEl=options.slideToggle.listenerEl;
        let event=options.slideToggle.event;
        let toggleEl=options.slideToggle.toggleEl;
        let listenerElUp=options.slideToggle.listenerElUp;
        
        $(listenerEl).on(event, function(e){
            $(toggleEl).slideDown();
        });
        $(listenerElUp).on(event, function(e){
            $(toggleEl).slideUp();
        });
    }
};