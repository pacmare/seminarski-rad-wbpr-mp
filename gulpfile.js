const
    //development mode
    dev = true,

    //modules
    gulp = require('gulp'),
    newer = require('gulp-newer'),
    htmlclean = require('gulp-htmlclean'),
    noop = require('gulp-noop'),
    concat = require('gulp-concat'),
    terser = require('gulp-terser'),
    rename = require('gulp-rename'),
    sync = require('browser-sync').create(),
    sass = require('gulp-sass')

    //folders
    src = 'src/',
    public = 'public/'


//HTML processing
function html() {
    return gulp.src(src + 'html/**/*')
        .pipe(newer(public))
        .pipe(dev ? noop() : htmlclean())
        .pipe(gulp.dest(public))
        .pipe(sync.stream());
}

//CSS processing
function css() {
    const out = public + 'css';
    return gulp.src(src + 'scss/main.scss')
        .pipe(sass({
            errLogToConsole:dev,
            outputStyle: dev ? 'expanded' : 'compressed'
        }).on('error', sass.logError))
        .pipe(rename({ 
            suffix: '.min'
        }))
        .pipe(gulp.dest(out))
        .pipe(sync.stream()); 
}

//JS processing
function js() {
    const out = public + 'js';

    return gulp.src([  
        src + 'js/base/**/*',
        src + 'js/components/**/*',
        src + 'js/pages/**/*',
        src + 'js/main.js'
    ])
        .pipe(concat('app.js'))
        .pipe(terser())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(out))
        .pipe(sync.stream());
}

//WATCH process
function watch(done) {
    sync.init({
        server: {
            baseDir: './' + public
        }
    })

    gulp.watch(src + 'html/**/*', html);
    gulp.watch(src + 'scss/**/*', css);
    gulp.watch(src + 'js/**/*', js);

    sync.reload();

    done();

}

//single tasks
exports.html = html;
exports.css = css;
exports.js = js;
exports.watch = watch;


//build task (run all single tasks)
exports.build = gulp.parallel(exports.html,  exports.css,  exports.js);

//default task
exports.default = gulp.series(exports.build, exports.watch);
