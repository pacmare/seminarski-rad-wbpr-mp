SEMINARSKI WEB PROJEKT
--------------------------------------------------------
-> program: Web programer za razvoj korisničkog sučelja
-> izrada: Marija Prša
-> link na projekt: https://gitlab.com/pacmare/seminarski-rad-wbpr-mp
--------------------------------------------------------
*************OPIS PROJEKTA*************
--------------------------------------------------------
* projekt je napravljen na temu "salon za masažu"
* za izradu sam koristila BOOTSTRAP v4.5
* za dodatno upravljanje elementima na stranicama koristila sam vlastite css klase i js funkcije te pluginove za dodatnu funkcionalnost
* svaka stranica se sastoji od zaglavlja sa navigacijskom trakom, glavnog sadržaja i podnožja
	-> za zaglavlje je korišten element Jumbotron unutar kojeg sam dodala navigaciju - subjektivan izgled
* web sjedište se sastoji od 7 stranica: 
	-> index, usluge, cjenik, poklon bon, lokacija, kontakt i rezervacija
* index stranica je početna i sadrži video element - za konverziju videa koristila sam Online-convert.com
* stranica s uslugama je samo jedna na kojoj se nalazi popis usluga, a pristupa se pomoću padajućeg izbornika u glavnom izborniku
	-> na taj način nastojala sam izbjeći prevelik broj stranica na samom sjedištu
	-> za pristup pojedinim dijelovima stranice koristila sam sekcije i sidra
	-> koristila sam sjene i tranzicije na slikama
* stranica s cjenikom sadrži tablicu s cjenama usluga na koju sam primjenila gradient
* poklon bon stranica prikazuje bon za odabir
	-> za simulaciju odabira koristila sam primjenu css klase i hide/show gumbom
* lokacija prikazuje stranicu na kojoj je prikazana lokacija salona pomoću Google Maps i kartica s adresom
	-> za primjer sam uzela lokaciju Glavnog kolodvora u Zagrebu
	-> na karti klikom na marker prikazuje se adresa
	-> na karticu s adresom primjenila sam animaciju koja se pokreće pri dolasku na stranicu i zaustavlja nakon 1 izvršavanja
* kontakt je stranica koja sadrži kontakt obrazac za komunikaciju korisnika
	-> za validaciju sam koristila Parsleyjs
	-> slanje obrasca odvija se putem mailto prema uputama za izradu semirnarskog rada
	-> obrazac je napravljen prema predlošku s elementima u uputama za izradu seminarskog rada
	-> za reset obrasca koristim JS funkciju
* rezervacija termina pokreće se gumbom Rezervacija na glavnom izborniku
	-> za simulaciju unutar modala korisnik može odabrati termin pomoću kalendara i vrstu usluge
	-> za kalendar koristim Full calendar
--------------------------------------------------------
--------------------------------------------------------
***CSS KLASE***
--------------------------------------------------------
* za izradu CSS-a korišten BOOTSTRAP v4.5 i Sass 7-1 Pattern
* unutar Sass strukture mapa Abstracts umjesto mape Helpers (tako smo radili na tečaju)
--------------------------------------------------------
***JS FUNKCIJE***
--------------------------------------------------------
* location.js - JS za prikaz lokacije
* contact.js - JS za kontakt obrazac
* giftcard.js - JS za poklon bon
--------------------------------------------------------
***PLUGINS***
--------------------------------------------------------
* Google API
* Parsleyjs
* Full calendar
* Google Font - Roboto Slab
--------------------------------------------------------
***MATERIJALI***
--------------------------------------------------------
* tekst -> Lorem Ipsum generator
* slike -> Pexels i Pixabay
* ikona -> Flaticon
* video -> Vimeo-Free-Videos, Online-convert.com